@extends('templates.master')

@section('content')

    <h2>Update Data</h2>
    <hr/>
    <a class="btn btn-primary" href="/services" style="margin-bottom: 15px;">Read Data</a>

    {!! Form::open(['id' => 'dataForm', 'method' => 'PATCH', 'url' => 'services/' . $service->id ]) !!}
    <div class="form-group">
        {!! Form::label('service_name', 'Service Name'); !!}
        {!! Form::text('service_name', $service->service_name , ['placeholder' => 'Enter service name', 'class' => 'form-control']); !!}
    </div>

    <div class="form-group">
        {!! Form::label('client_name', 'Client Name'); !!}
        {!! Form::text('client_name', $service->client_name , ['placeholder' => 'Enter client name', 'class' => 'form-control']); !!}
    </div>

    <div class="form-group">
        {!! Form::label('email', 'Client Email') !!}
        {!! Form::email('email', $service->email, ['placeholder' => 'Enter email', 'class' => 'form-control']); !!}
    </div>

    <div class="form-group">
        {!! Form::label('mobile', 'Phone'); !!}
        {!! Form::text('mobile', $service->phone, ['placeholder' => 'Enter phone', 'class' => 'form-control']); !!}
    </div>

    <div class="form-group">
        {!! Form::label('address', 'Address'); !!}
        {!! Form::text('address', $service->address, ['placeholder' => 'Enter address', 'class' => 'form-control']); !!}
    </div>

    {!! Form::submit('Update', ['class' => 'btn btn-primary pull-right']); !!}

    {!! Form::close() !!}
@endsection()